// centralize all exports in this index file
module.exports = {
  htmlGenerator: require('./htmlGenerator'),
  restRoutes: require('./rest/')
}
