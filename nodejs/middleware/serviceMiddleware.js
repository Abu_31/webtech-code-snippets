let services = require('../services');

// our middleware injects all services directly in the request object
function serviceMiddleware() {
  return function(req,res,next) {
    req.services = services;
    next();
  };
}

module.exports = serviceMiddleware;
