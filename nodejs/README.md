# Node Horae

## Voraussetzungen

Zum Downloaden der Beispiele sollte git benutzt werden. Zum Ausführen muss NodeJS installiert sein und im Systempfad/Environment eingetragen sein. Zusätzlich benötigen wir für unser Projekt MongoDB.

#### NodeJS Installation

NodeJS kann auf allen Systemen manuell von https://nodejs.org/de/ heruntergeladen und installiert werden. NodeJS sollte sich hierbei selbstständig in den Systempfad/das Environment eintragen um von CMD oder Terminal aus benutzt werden zu können. Sollte dies nicht passieren muss dies selbst per Hand eingetragen werden.

Unter Linux & Mac kann eine aktuelle NodeJS-Version meist auch über den passenden package manager(apt, yum, brew, ...) bezogen werden.

#### Erweiterte NodeJS Installation via NVM
NVM ist ein Verwaltungstool für verschiedene Versionen von NodeJS.
Linux- und Mac-Nutzer können diesen z.b. über [Github.com/creationix/NVM](https://github.com/creationix/nvm) beziehen. Windows-Nutzer können eine äquivalente Version von [Github.com/coreybutler/NVM](https://github.com/coreybutler/nvm-windows) beziehen.

#### MongoDB Installation

MongoDB kann auf allen Systemen manuell von https://www.mongodb.com/download-center heruntergeladen und installiert werden.

Unter Windows kann mongodb (wenn erfolgreich im Systempfad eingetragen) z.b. wie folgt gestartet werden:
```
mongod --port 27017 --dbpath C:\MongoDB\data\db
```

Zusätzlich kann MongoDB auf Linux & Mac meist über den passenden package manager bezogen werden. Hier wird MongoDB meist mit dem System automatisch gestartet! Ansonsten könnt ihr MongoDB über die passenden Befehle (`mongod`) starten.


## Projekt Installation
```
git clone https://git.rwth-aachen.de/WebTechnologies/node.horae
cd node.horae
npm install
```

## Ausführung des Projects
```
npm start
```
und anschließend den Browser öffnen: http://localhost:8080/
