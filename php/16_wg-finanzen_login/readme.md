Zum Zurücksetzen der Datenbank muss im Unterverzeichnis data ein Webserver gestartet und das PHP-Skript wgfinanzen.php ausgeführt werden.

1. Wechsle nach data
2. Öffne Terminal
3. Starte Webserver mit
	php -S localhost:8888
4. Öffne Browser
5. Gehe zu http://localhost:8888/wgfinanzen.php
6. Es sollte "Database reset done!" erscheinen

Nach dem Zurücksetzen der Datenbank sind die Zugangsdaten wie folgt:

Name: Alice
Passwort: alice

Name: Bob
Passwort: bob

Name: Eve
Passwort: eve