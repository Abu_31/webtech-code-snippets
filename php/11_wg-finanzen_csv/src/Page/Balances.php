<?php
namespace WGFinanzen\Page;

require_once(__DIR__.'/PageInterface.php');
require_once(__DIR__.'/../Data.php');
require_once(__DIR__.'/../Data/Purchase.php');

use WGFinanzen\Data;

class Balances implements PageInterface{

    /** @var  Data */
    protected $data;

    function __construct(Data $data)
    {
        $this->data = $data;
    }

    private function setupBalancesArray(){
        $balances = [];
        $flatMates = $this->getData()->getAllFlatMates();
        foreach($flatMates as $flatmate){
            $balances[$flatmate->getID()] = [];
            foreach ($flatMates as $flatMate){
                $balances[$flatmate->getID()][$flatMate->getID()] = 0;
            }
        }
        return $balances;
    }

    private function setupBalancesNameArray($balances){
        $balancesNameArray = [];
        foreach ($balances as $flatmateID => $flatmaterow){
            foreach ($flatmaterow as $id => $balance){
                if($balance > 0){
                    $balancesNameArray[] = [
                        $this->getData()->getFlatMate($flatmateID)->getName(),
                        $this->getData()->getFlatMate($id)->getName(),
                        $balance
                        ];
                }
            }
        }
        return $balancesNameArray;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getTitle()
    {
        return 'Bilanzen';
    }

    public function getViewScript()
    {
        return __DIR__.'/../../view/balances.phtml';
    }

    public function getViewVariables()
    {
        return [
            'balances' => $this->getBalances(),
            'flatMates' => $this->getData()->getAllFlatMates()
        ];
    }

    protected function getBalances(){
        $purchases = $this->getData()->getAllPurchases();
        $balances = $this->setupBalancesArray();
        foreach($purchases as $purchase){
            $dif = $purchase->getCost()/count($purchase->getBoughtFor());
            foreach($purchase->getBoughtFor() as $flatmate){
                if($flatmate->getID() != $purchase->getBoughtBy()->getID()){
                    $balances[$flatmate->getID()][$purchase->getBoughtBy()->getID()] =
                        $balances[$flatmate->getID()][$purchase->getBoughtBy()->getID()] + $dif;
                }
            }
        }
        $balances = $this->setupBalancesNameArray($balances);
        /*
        foreach($flatMates as $flatMate){
            $sum = 0;
            foreach($purchases as $purchase){
                if($purchase->getBoughtBy() == $flatMate && !empty($purchase->getBoughtFor())){
                    $sum += $purchase->getCost();
                }
                if(in_array($flatMate, $purchase->getBoughtFor())){
                    $sum -= $purchase->getCost() / count($purchase->getBoughtFor());
                }
            }
            $balances[$flatMate->getId()] = $sum;
        }//*/
        return $balances;
    }
}