import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddFlatmateComponent } from './add-flatmate/add-flatmate.component';
import { AddPurchaseComponent } from './add-purchase/add-purchase.component';
import { ShowPurchasesComponent } from './show-purchases/show-purchases.component';
import { BalancesComponent } from './balances/balances.component';

const routes: Routes = [
    { path: '', component: AddFlatmateComponent },
    { path: 'addPurchase', component: AddPurchaseComponent },
    { path: 'showPurchases', component: ShowPurchasesComponent },
    { path: 'balances', component: BalancesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
