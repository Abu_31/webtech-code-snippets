import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Flatmate } from './flatmate';
import { Purchase } from './purchase';

@Injectable({
  providedIn: 'root'
})
export class HttpCommunicatorService {

  //apiUrl: string = 'http://localhost:8080/api/v1/';
  apiUrl: string = '/api/v1/';
  flatmateUrl: string = 'flatmates';
  purchaseUrl: string = 'purchases';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  public getAllFlatmates(): Observable<HttpResponse<Flatmate[]>> {
    return this.http.get<Flatmate[]>(this.apiUrl + this.flatmateUrl, { observe: 'response' });
  }
  
  public addFlatmate(newFlatmate: Flatmate): Observable<HttpResponse<Flatmate>> {
    return this.http.post<Flatmate>(this.apiUrl + this.flatmateUrl, newFlatmate, { headers: this.httpOptions.headers, observe: 'response'});
  }

  public getAllPurchases(): Observable<HttpResponse<Purchase[]>> {
    return this.http.get<Purchase[]>(this.apiUrl + this.purchaseUrl, {observe: 'response'});
  }

  public addPurchase(newPurchase: Purchase): Observable<HttpResponse<Purchase>> {
    return this.http.post<Purchase>(this.apiUrl + this.purchaseUrl, newPurchase, { headers: this.httpOptions.headers, observe: 'response' });
  }

  public deletePurchase(id: number): Observable<HttpResponse<Purchase>> {
    return this.http.delete<Purchase>(this.apiUrl + this.purchaseUrl + '/' + id, { observe: 'response' });
  }

}
